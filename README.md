# Servers
---

| #  |  | Hostname            | IP              |  | Hostname            | IP             |
|----|--|---------------------|-----------------|--|---------------------|----------------|
| 1  |  | win-vsts-agent-0jlp | 104.199.19.222  |  | win-vsts-agent-h6dq | 34.78.88.227   |
| 2  |  | win-vsts-agent-1cz7 | 35.233.103.151  |  | win-vsts-agent-k3db | 34.140.156.181 |
| 3  |  | win-vsts-agent-1d1k | 104.155.82.217  |  | win-vsts-agent-k7zn | 34.79.29.249   |
| 4  |  | win-vsts-agent-2fbj | 104.155.55.186  |  | win-vsts-agent-kgzm | 104.199.31.55  |
| 5  |  | win-vsts-agent-2p6k | 34.79.30.97     |  | win-vsts-agent-l2rg | 35.240.45.242  |
| 6  |  | win-vsts-agent-3fn8 | 34.76.217.221   |  | win-vsts-agent-mbpb | 34.78.97.8     |
| 7  |  | win-vsts-agent-409j | 34.77.234.118   |  | win-vsts-agent-n119 | 34.140.105.246 |
| 8  |  | win-vsts-agent-40z1 | 35.195.251.246  |  | win-vsts-agent-nm3r | 34.79.224.125  |
| 9  |  | win-vsts-agent-57x0 | 34.79.58.94     |  | win-vsts-agent-p42x | 35.233.84.99   |
| 10 |  | win-vsts-agent-5ppg | 34.78.148.5     |  | win-vsts-agent-qncx | 34.78.253.73   |
| 11 |  | win-vsts-agent-6jqw | 35.189.243.35   |  | win-vsts-agent-rts6 | 35.187.93.67   |
| 12 |  | win-vsts-agent-8bxz | 35.195.154.201  |  | win-vsts-agent-s4cn | 34.140.192.148 |
| 13 |  | win-vsts-agent-8c7p | 34.78.189.253   |  | win-vsts-agent-sfdg | 34.78.170.129  |
| 14 |  | win-vsts-agent-b6mp | 34.78.207.100   |  | win-vsts-agent-tjg5 | 34.78.36.44    |
| 15 |  | win-vsts-agent-bs1k | 34.78.209.148   |  | win-vsts-agent-tmgd | 34.76.170.85   |
| 16 |  | win-vsts-agent-cr46 | 35.205.177.79   |  | win-vsts-agent-vt2d | 34.78.42.208   |
| 17 |  | win-vsts-agent-d3d4 | 34.79.101.220   |  | win-vsts-agent-w39w | 35.205.165.90  |
| 18 |  | win-vsts-agent-d7h3 | 104.155.106.239 |  | win-vsts-agent-w8sl | 34.78.19.59    |
| 19 |  | win-vsts-agent-f29v | 192.158.31.33   |  | win-vsts-agent-wmcg | 35.195.163.66  |
| 20 |  | win-vsts-agent-gdpl | 34.140.107.84   |  | win-vsts-agent-zv18 | 34.140.10.36   |
